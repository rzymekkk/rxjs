import { Component, OnInit, ViewChild, NgZone } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';

const baseUrl = 'https://rzymek.github.io/rzymekon/BrendanEich/followers';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  result = undefined;
  subscription = Observable.empty().subscribe();
  lastMod = 0
 
  constructor(private http: Http, private zone: NgZone) {
  }
  run() {
    this.result = 'Loading...';
    this.subscription.unsubscribe();
    this.subscription = this.http.get(baseUrl)
      .map(resp => resp.json())
      .switch() //switch from array of users to stream of user objects
      .map(user => user.url)
      .map(url => url + "?modAfter="+this.lastMod)
      // .map((url,idx) => idx === 5 ? '/bla' : url)
      .map(url => this.http.get(url))
      /*
      .map(url => this.http.get(url)
        .catch(e => Observable.of({
          json() {
            return { location: `${e.url}: ${e.statusText}` }
          }
        }))
      )
      */
      .mergeAll() // .concatAll
      .map(resp => resp.json())
      .map(user => user.location)
      .filter(v => v)
      .toArray()
      .subscribe({
        next: v => this.showResult(v),
        error: error => this.showResult({error:error}),
        complete: () => {
          console.log('done');
          this.lastMod = new Date().getTime()
        }
      })
  }

  showResult(result: {}) {
    // console.log(result);
    this.zone.run(() => {
      this.result = JSON.stringify(result, null, ' ');
    })
  }

}
